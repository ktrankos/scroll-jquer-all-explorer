$(window).load(init);
var ACT = 0;
var PREV =0;
var isMoving = false;
///var for web
var isWEB =false;
var initWEB =false;
///vars for mobile
var isMobile = false;
var initMOBILE = false;

var _envACT ='WEB';
var _envPREV = 'WEB';

var Skrollr = null;
var scrollItems;

function init()
{
	Routing();
	isWEB = (!Modernizr.mq('only all and (max-width: 728px)'));
	isMobile = !isWEB;
	_envACT = (isWEB) ? "WEB" : "MOBILE";
	_envPREV = _envACT;


	if(isWEB) initWeb();
	else initMobile();
	$('.onepage-pagination li a').bind('click', function()
	{
		PREV =ACT;
		$(this).parent().parent().find('li').removeClass('active');
		$(this).parent().addClass('active');
		ACT = $(this).parent().index();

		if(isWEB) moveEl();
		if(isMobile) scrollWindow();
		
	});
	scrollItems = $('.onepage-pagination li a').map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });

	$(window).bind('scroll', function(){
		var scrollPos = $(this).scrollTop();
		$('a.nav').each(function () 
		{			
			var currLink = $(this); 
			var i = $(this).parent().index();
			var refElement = $('.el:eq('+i+')');			
			if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) 
			{
				if(i!=ACT){
					PREV = ACT;
					ACT = i;
				}
				
				currLink.parent().addClass("active");
			}
			else{
				currLink.parent().removeClass("active");
			}
		});
	});
	$(window).resize(onResize);
	doAnim();
	onResize();
	
}
function Routing()
{
	var route = location.href
	if(route.lastIndexOf("#")==-1) return false;
	
	var uri = /\#\w+/.exec(route);
	var el = $('a.nav[href="'+uri+'"]')
	ACT= el.parent().index();
	$('.onepage-pagination li').removeClass('active');
	el.parent().addClass('active');
	

}
function initWeb()
{
	initWEB = true;
	$('body, html').scrollTop(0);
	if(Skrollr!=null) Skrollr.destroy();
	Skrollr = null;
	
	
	$('body').mousewheel(function(objEvent, intDelta)
	{
		if(!isMoving)
		{
			if(!isWEB) return false;//bloqueado a la vieja usanza
			
			PREV = ACT;
			ACT = (intDelta < 0)? ACT+1: ACT-1 ;
			if(ACT<0) ACT = 0;
			if(ACT > $('.el').length-1) ACT = $('.el').length-1;
			$('.onepage-pagination li').removeClass('active');
			$('.onepage-pagination li:eq('+ACT+')').addClass('active');
			moveEl();
		}
	});
	
}
function initMobile()
{
	initMOBILE = true;
	if(Skrollr==null) Skrollr = skrollr.init();
	$('.cont-el, .el .int').removeAttr('style');
	
	var _pos = $('.el:eq('+ACT+')').position();	
	$(window).scrollTop(_pos.top);
	
	

	
}
function defineType()
{
	isWEB = (!Modernizr.mq('only all and (max-width: 728px)'));
	isMobile = !isWEB;
	_envACT = (isWEB) ? "WEB" : "MOBILE";
	if(isWEB && _envACT!=_envPREV) initWeb();
	if(isMobile  && _envACT!=_envPREV) initMobile();
	_envPREV = _envACT;
}

/*---------------------------------------------------------------------------------------------------------*/

function moveEl()
{
	isMoving = true;
	var _h = $(window).height();
	var _to = ACT * _h*-1;
	
	TweenLite.to($('.cont-el'), 0.6, {marginTop:_to+'px', ease: Power1.easeInOut, onComplete:function(){
		isMoving=false;
		doAnim();
	}});
}
function doAnim()
{
	var _cont =$('.el:eq('+ACT+')');
	if(ACT==0) anim_1(_cont);
	reset();
}
function vCenter(_h)
{	
	if(isMobile)
	{
		$('.el .int').css({marginTop: 0});
		return false;
	}
	var _top = _h/2-380;
	$('.el .int').css({marginTop: _top});

}
function reset()
{
	var _cont =$('.el:eq('+PREV+')');
	_cont.find('.anim-hide').css({opacity:0});
}
function onResize()
{	
	defineType();
	if(isWEB) resizeWEB(); 
	else resizeMobile();

	
}
//////////////////////////RESIZEROS
function resizeWEB()
{
	var _h = $(window).height();
	vCenter(_h);	
	$('.cont-el').css({marginTop:ACT* _h*-1});
}
function resizeMobile()
{
	
}
function scrollWindow()
{
	var _pos = $('.el:eq('+ACT+')').position();	
	$('body, html').animate({scrollTop:_pos.top}, 400);
}
///////////////////////////////////
function anim_1(_cont)
{
	TweenLite.fromTo(_cont.find('.anim-hide'), 0.7, {css:{alpha:0}}, {css:{alpha:1, delay: 0.8}});
}